CFLAGS=-Wall -g

all: clean lowercase

clean:
	find . -maxdepth 1 -type f -executable -delete

run: all
	gdb lowercase

build: clean
	${CC} -Wall -O2 -o lowercase lowercase.c
	x86_64-w64-mingw32-gcc -o lowercase.exe lowercase.c
